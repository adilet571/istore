class CartsController < InheritedResources::Base
skip_before_filter :authorize

  def show
    begin
    @cart = Cart.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      logger.error "Attempt to access invalid cart id #{params[:id]}"
      redirect_to store_index_url,notice: 'Invalid Cart'
    else
      respond_to do |format|
        format.html
        format.json {render json: @cart}
      end
    end

  end

  def destroy
    @cart = current_cart
    @cart.destroy
    session[:cart_id] = nil
    respond_to do |format|
      format.html {redirect_to store_index_url}
      format.json {head :no_content}
    end
  end

  private

    def cart_params
      params.require(:cart).permit()
    end
end

