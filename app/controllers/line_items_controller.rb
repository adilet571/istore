class LineItemsController < InheritedResources::Base
  skip_before_filter :authorize
  def create
    @cart = current_cart
    product = Product.find(params[:product_id])
    @line_item = @cart.add_product(product.id)
    respond_to do |format|
      if @line_item.save
        format.html { redirect_to store_index_url}
        format.js   { @current_item = @line_item }
        format.json { render json: @line_item,
                             status: :created, location: @line_item }
      else
        format.html { render action: "new" }
        format.json { render json: @line_item.errors,
                             status: :unprocessable_entity }
      end
    end
  end


  private

    def line_item_params
      params.require(:line_item).permit(:product_id, :cart_id)
    end
end

