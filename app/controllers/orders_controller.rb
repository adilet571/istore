class OrdersController < InheritedResources::Base
  skip_before_filter :authorize

  def index
    @orders = Order.paginate(page: params[:page],per_page: 10)
    respond_to do |format|
      format.html
      format.json{render json: @orders}
    end
  end


  def new
    @cart = current_cart
    if @cart.line_items.empty?
      redirect_to store_index_url, notice: 'Your Cart is empty!'
      return
    end

    @order = Order.new
    respond_to do |format|
      format.html
      format.json{render json: @order}
    end

  end

  def create
    @order = Order.new(order_params)
    @order.add_line_items_from_cart(current_cart)
    respond_to do |format|
      if @order.save
        Cart.destroy(session[:cart_id])
        session[:cart_id] = nil
        OrderNotifier.received(@order).deliver
        format.html{redirect_to store_index_url,notice: 'Thank you for your order'}
        format.json{render json: @order,status: :created,location: @order}
      else
        @cart = current_cart
        format.html{redirect_to action: 'new'}
        format.json{render json: @order.errors, status: :unprocessable_entity}
      end

    end
  end

  private

    def order_params
      params.require(:order).permit(:name, :address, :email, :pay_type)
    end
end

