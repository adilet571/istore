class ProductsController < InheritedResources::Base


  def index
    @products = Product.all
  end

  def who_bought
    @product = Product.find(params[:id])
    respond_to do |format|
      format.atom
    end
  end

  private

    def product_params
      params.require(:product).permit(:title, :description, :image_url, :price)
    end
end

