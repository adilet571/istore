class RegisterController < ApplicationController

  def confirm
   @user = User.find_by_confirm_hash(params[:hash])
    if !@user.nil?
      # @user.is_confirmed = 1
      if @user.update(is_confirmed: 1)
         respond_to do |format|
          format.html{redirect_to root_url,notice: 'Confirmation activated!'}
         end
      end
    else
      redirect_to store_index_url,notice:  'Confirmation failed!'
    end
  end
end
