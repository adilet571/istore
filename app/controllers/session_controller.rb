class SessionController < ApplicationController
  skip_before_filter :authorize
  def create
    user = User.find_by_name(params[:name])
    if user and user.authenticate(params[:password])
      session[:user_id] = user.id
      redirect_to store_index_url;
    else
      redirect_to login_url,alert: 'Invalid username or password'
    end
  end

  def destroy
    session[:user_id] = nil
    redirect_to store_index_url,notice: 'Logged out'
  end

  def new

  end
end
