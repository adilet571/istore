class ConfirmationNotifier < ApplicationMailer

  default from:'Adilet Joldoshbekov <adilet.571@gmail.com>'
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.confirmation_notifier.send.subject
  #
  def send1(user)
    @user = user
    @greeting = "Hi"

    mail to: @user.email
  end
end
