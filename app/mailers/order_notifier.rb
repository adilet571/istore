class OrderNotifier < ApplicationMailer

  default from:'Adilet Joldoshbekov <adilet.571@gmail.com>'
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.order_notifier.received.subject
  #
  def received(order)
    @order = order
    @greeting = "Hi"

    mail(:to => order.email, subject: 'IStore')
  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.order_notifier.shipped.subject
  #
  def shipped
    @greeting = "Hi"

    #mail (to: order.email,subject: 'IStore')
  end
end
