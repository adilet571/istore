# Preview all emails at http://localhost:3000/rails/mailers/confirmation_notifier
class ConfirmationNotifierPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/confirmation_notifier/send
  def send
    ConfirmationNotifier.send
  end

end
